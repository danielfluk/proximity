import express from 'express';
import { PORT } from './configurations/constants';
import userRouter from './routes/user.route';

const app = express();
app.use(express.json());

app.use('/users', userRouter);

app.listen(PORT, () => {
  console.log(`Server is listening on port ${PORT}`);
});
