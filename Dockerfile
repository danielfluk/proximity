FROM node
USER node
WORKDIR /home/node/app
COPY server/node_modules node_modules
COPY server/dist .
EXPOSE 8080

CMD ["node", "src/index.js"]